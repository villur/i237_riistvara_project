#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "../lib/andygock_avr-uart/uart.h"
#include <avr/pgmspace.h>

void print_ascii_tbl()
{
    for (char c = ' '; c <= '~'; c++) {
        uart0_putc(c);
    }

    uart0_puts_p(PSTR("\r\n"));
}

void print_for_human (const unsigned char *array, const size_t len)
{
    for (unsigned int i = 0; i < len; i++) {
        if (array[i] >= ' ' && array[i] <= '~') {
            uart0_putc(array[i]);
        } else {
            uart0_puts_p(PSTR("\"0x"));
            uart0_putc((array[i] >> 4) + ((array[i] >> 4) <= 9 ? 0x30 : 0x37));
            uart0_putc((array[i] & 0x0F) + ((array[i] & 0x0F) <= 9 ? 0x30 : 0x37));
            uart0_putc('"');
        }
    }

    uart0_puts_p(PSTR("\r\n"));
}


