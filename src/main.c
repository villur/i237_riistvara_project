#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdio.h>
#define __ASSERT_USE_STDERR
#include <assert.h>
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/hd44780_111/hd44780.h"
#include <avr/pgmspace.h>
#include <string.h>
#include <stdbool.h>
#include "print_helper.h"
#include "hmi_msg.h"
#include "cli_microrl.h"
#include "../lib/helius_microrl/microrl.h"
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <time.h>

#define RED PORTA0
#define GREEN PORTA2
#define BLUE PORTA4

#define UART_BAUD           9600
#define UART_STATUS_MASK    0x00FF

//  Create  microrl object  and pointer on  it
microrl_t rl;
microrl_t *prl = &rl;


void light_led(int port)
{
    PORTA |= _BV(port);
    _delay_ms(100);
    PORTA &= ~_BV(port);
    _delay_ms(100);
}

static inline void init_leds (void)
{
    DDRB |= _BV(DDB7);
    DDRA |= _BV(DDA0);
    DDRA |= _BV(DDA2);
    DDRA |= _BV(DDA4);
}

static inline void init_errcon(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_puts_p(studname);
    uart0_puts_p(PSTR("\r\n-------\r\n"));
    microrl_init(prl, uart0_puts);
    microrl_set_execute_callback(prl, cli_execute);
}

static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= _BV(CS12); // fCPU/256
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}

static inline void print_sys_time(void)
{
    static time_t prev_time;
    time_t now = time(NULL);

    if (prev_time != now) {
        char iso_time[20] = {0x00};
        struct tm now_tm;
        time_t now = time(NULL);
        gmtime_r(&now, &now_tm);
        isotime_r(&now_tm, iso_time);
        uart1_puts(iso_time);
        uart1_puts_p(PSTR("\r\n"));
        prev_time = now;
    }
}

static inline void heartbeat(void)
{
    static time_t prev_time;
    char ascii_buf[11] = {0x00};
    time_t now = time(NULL);

    if (prev_time != now) {
        //Print uptime to uart1
        ltoa(now, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s.\r\n"));
        //Toggle LED
        PORTA ^= _BV(GREEN);
        prev_time = now;
    }
}



void blink_leds(void)
{
    int r = rand() % 3;

    switch (r) {
    case 0:
        light_led(GREEN);
        break;

    case 1:
        light_led(BLUE);
        break;

    case 2:
        light_led(RED);
        break;

    default:
        break;
    }
}


void main(void)
{
    //  Call init with ptr to microrl instance and print callback
    microrl_init(prl, uart0_puts);
    //  Set callback    for execute
    microrl_set_execute_callback(prl, cli_execute);
    init_leds();
    init_errcon();
    init_sys_timer();
    sei();
    lcd_init();
    lcd_home();
    lcd_puts_P(studname);

    while (1) {
        //  CLI commands    are handled in  cli_execute()
        heartbeat();
        print_sys_time();
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
    }
}
ISR(TIMER1_COMPA_vect)
{
    system_tick();
}