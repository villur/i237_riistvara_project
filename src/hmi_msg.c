#include <avr/pgmspace.h>


const char studname[] PROGMEM = "Villem M. Loigom";
const char one[] PROGMEM = "One";
const char two[] PROGMEM = "Two";
const char three[] PROGMEM = "Three";
const char four[] PROGMEM = "Four";
const char five[] PROGMEM = "Five";
const char six[] PROGMEM = "Six";
const char seven[] PROGMEM = "Seven";
const char eight[] PROGMEM = "Eight";
const char nine[] PROGMEM = "Nine";
const char zero[] PROGMEM = "Zero";
PGM_P const numbrid[] PROGMEM = {
    zero,
    one,
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine
};